<?php

namespace Giift\CurrencyConverter\Providers;

use Monolog\Logger;
use Http\Client\HttpClient;
use Psr\Log\LoggerInterface;
use Http\Discovery\HttpClientDiscovery;
use Giift\CurrencyConverter\Traits\HasLogger;
use Giift\CurrencyConverter\Traits\HasHttpClient;

/**
 * Created by PhpStorm.
 * User: mquinn
 * Date: 11/10/17
 * Time: 11:38 AM
 */

/**
 * Class Provider
 * @package Giift\CurrencyConverter\Providers
 */
abstract class Provider implements IProvider
{
    use HasLogger;
    use HasHttpClient;

    /**
     * Provider constructor.
     * @param LoggerInterface|null  $logger
     * @param HttpClient|null       $client
     */
    public function __construct(LoggerInterface $logger = null, HttpClient $client = null)
    {
        //Setting the logger.
        if (is_null($logger)) {
            $logger = new Logger(static::class);
        }
        $this->setLogger($logger);

        //Setting the Http Client
        if (is_null($client)) {
            $client = HttpClientDiscovery::find();
        }
        $this->setClient($client);
    }

    /**
     * Returns currency rate.
     * @param string    $fromCurrency
     * @param string    $toCurrency
     * @return float
     */
    abstract public function getRate($fromCurrency, $toCurrency);
}
