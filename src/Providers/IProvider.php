<?php

namespace Giift\CurrencyConverter\Providers;

use Http\Client\HttpClient;
use Psr\Log\LoggerInterface;

/**
 * Interface IProvider
 * @package Giift\CurrencyConverter\Providers
 */
interface IProvider
{
    /**
     * IProvider constructor.
     * @param HttpClient|null $client
     * @param LoggerInterface|null $logger
     */
    public function __construct(LoggerInterface $logger = null, HttpClient $client = null);

    /**
     * @param $fromCurrency
     * @param $toCurrency
     * @return float
     */
    public function getRate($fromCurrency, $toCurrency);
}