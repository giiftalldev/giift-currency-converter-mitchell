<?php

namespace Giift\CurrencyConverter;

use Monolog\Logger;
use Giift\Culture\Culture;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;
use Giift\CurrencyConverter\Traits\HasLogger;
use Giift\CurrencyConverter\Traits\HasCachehandler;
use Giift\CurrencyConverter\Providers\MockProvider;
use Symfony\Component\Cache\Simple\FilesystemCache;
use Giift\CurrencyConverter\Providers\IProvider;

/**
 * Class CurrencyConverter
 * @package Giift\CurrencyConverter
 */
class CurrencyConverter
{
    use HasLogger;
    use HasCachehandler;

    /** @var  IProvider */
    private $provider;

    /** 21600 = 6 hours */
    const TTL = 21600;

    /**
     * CurrencyConverter constructor.
     * @param LoggerInterface|null $logger
     * @param IProvider|null $provider
     * @param CacheInterface|null $cachehandler
     */
    public function __construct(
        LoggerInterface $logger = null,
        IProvider $provider = null,
        CacheInterface $cachehandler = null
    ) {
        //Setting the logger.
        if (is_null($logger)) {
            $logger = new Logger(static::class);
        }
        $this->setLogger($logger);

        //Setting the provider
        if (is_null($provider)) {
            $provider = new MockProvider($this->getLogger());
        }
        $this->setProvider($provider);

        //Setting the cachehandler.
        if (is_null($cachehandler)) {
            $cachehandler = new FilesystemCache('CurrencyConverter');
        }
        $this->setCachehandler($cachehandler);
    }


    /**
     * @param string    $from   Currency CCY.
     * @param string    $to     Currency CCY.
     * @param float|int $amount Amount to convert by rate.
     * @return float
     * @throws \Exception
     */
    public function convert($from, $to, $amount = null)
    {
        //Forcing the ccys to upper for simplicity.
        $from = strtoupper($from);
        $to = strtoupper($to);

        //Using culture class to check if the ccy is valid.
        $culture = Culture::instance();

        //Safety checks for non-valid ccy.
        if (!$culture->validAlpha3($from)) {
            $this->getLogger()->error("CurrencyConverter - convert - $from is not valid ccy");
            throw new \InvalidArgumentException("CurrencyConverter - convert - $from is not valid ccy");
        }

        if (!$culture->validAlpha3($to)) {
            $this->getLogger()->error("CurrencyConverter - convert - $to is not valid ccy");
            throw new \InvalidArgumentException("CurrencyConverter - convert - $to is not valid ccy");
        }

        //Key for caching system.
        $cacheKey = "$from$to";

        //Checking the cache to see if the key exists.
        if ($this->getCachehandler()->has($cacheKey)) {
            $rate = $this->getCachehandler()->get($cacheKey);
        } else {
            //Getting the rate from the provider.
            $rate = $this->getProvider()->getRate($from, $to);
            if (is_null($rate)) {
                $this->getLogger()->error("CurrencyConverter - convert - Failed to get rate.");
                throw new \Exception("CurrencyConverter - convert - Failed to get rate.");
            }
            $this->getCachehandler()->set($cacheKey, $rate, static::TTL);
        }

        //If the amount is null we will just return the rate.
        if (is_null($amount)) {
            return $rate;
        }

        //Safety check to make sure the amount is numeric.
        if (!is_numeric($amount)) {
            $this->getLogger()->error("CurrencyConverter - convert - amount is not numeric.");
            throw new \InvalidArgumentException("CurrencyConverter - convert - amount is not numeric.");
        }

        //Finally just returning the rate * amount.
        return $rate * $amount;
    }

    /**
     * @param IProvider $provider
     * @return static
     */
    public function setProvider(IProvider $provider)
    {
        $this->provider = $provider;
        return $this;
    }

    /**
     * @return IProvider
     * @throws \Exception
     */
    protected function getProvider()
    {
        $provider = $this->provider;
        if (!is_a($provider, IProvider::class)) {
            $this->getLogger()->error('CurrencyConverter - getProvider - Provider class wrong.');
            throw new \Exception('CurrencyConverter - getProvider - Provider class wrong.');
        }
        return $provider;
    }
}
