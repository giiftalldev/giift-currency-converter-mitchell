<?php

namespace Giift\CurrencyConverter\Traits;

use Psr\SimpleCache\CacheInterface;
use Symfony\Component\Cache\Simple\FilesystemCache;

/**
 * Created by PhpStorm.
 * User: mquinn
 * Date: 11/10/17
 * Time: 3:34 PM
 */

/**
 * Trait HasCachehandler
 * @package Giift\CurrencyConverter\Traits
 */
trait HasCachehandler
{
    /** @var CacheInterface */
    protected $cachehandler;

    /**
     * @return CacheInterface
     * @throws \Exception
     */
    protected function getCachehandler()
    {
        $cachehandler = $this->cachehandler;
        if (is_null($cachehandler)) {
            $cachehandler = new FilesystemCache('CurrencyConverter');
            $this->setCachehandler($cachehandler);
        }
        return $cachehandler;
    }

    /**
     * @param CacheInterface $cachehandler
     * @return static
     */
    public function setCachehandler(CacheInterface $cachehandler)
    {
        $this->cachehandler = $cachehandler;
        return $this;
    }

}