<?php

namespace Giift\CurrencyConverter\Traits;

use Http\Discovery\HttpClientDiscovery;
use Http\Client\HttpClient;

/**
 * Created by PhpStorm.
 * User: mquinn
 * Date: 11/10/17
 * Time: 11:41 AM
 */

/**
 * Trait HasHttpClient
 * @package Giift\CurrencyConverter\Traits
 */
trait HasHttpClient
{
    /** @var  HttpClient */
    protected $client;

    /**
     * @return HttpClient
     */
    protected function getClient()
    {
        $client = $this->client;
        if (is_null($client)) {
            $client = HttpClientDiscovery::find();
            $this->setClient($client);
        }
        return $client;
    }

    /**
     * @param HttpClient $client
     * @return static
     */
    public function setClient(HttpClient $client)
    {
        $this->client = $client;
        return $this;
    }
}