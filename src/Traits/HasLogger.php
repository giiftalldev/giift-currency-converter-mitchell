<?php

namespace Giift\CurrencyConverter\Traits;

use Monolog\Logger;
use Psr\Log\LoggerInterface;

/**
 * Created by PhpStorm.
 * User: mquinn
 * Date: 11/10/17
 * Time: 11:31 AM
 */

/**
 * Trait HasLogger
 * @package Giift\CurrencyConverter\Traits
 */
trait HasLogger
{
    /** @var  LoggerInterface */
    protected $logger;

    /**
     * @return LoggerInterface
     */
    protected function getLogger()
    {
        $logger = $this->logger;
        if (is_null($logger)) {
            $logger = new Logger(static::class);
            $this->setLogger($logger);
        }
        return $logger;
    }

    /**
     * @param LoggerInterface $logger
     * @return static;
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
        return $this;
    }
}
