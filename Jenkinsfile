#!groovy
node {
  stage('Checkout'){
    echo('Cleaning...')
    step([$class: 'WsCleanup'])
    echo('Cloning...')
    checkout scm
    sh(returnStdout:true, script:'git submodule update --init')
  }

  stage('Prepare'){
    echo('Preparing Workspace')
    parallel(
      clean:{
        sh('ant prepare')
      },
      composer:{
        sh('ant composer')
      }
    )
  }

  stage('Analysis'){
    echo('Analysing Code')
    parallel (
      lint:{
        echo('Lint')
        sh('ant lint')
      },
      pdepend:{
        echo('Depend')
        sh('ant pdepend')
        archiveArtifacts(allowEmptyArchive: true, artifacts:'build/pdepend/dependencies.svg')
        archiveArtifacts(allowEmptyArchive: true, artifacts:'build/pdepend/overview-pyramid.svg')
      },
      phpmd:{
        echo('Mess')
        sh('ant phpmd-ci')
        // pmd publishing
        step([$class: 'PmdPublisher',
          canComputeNew: false,
          defaultEncoding: '',
          healthy: '',
          pattern: 'build/logs/pmd.xml',
          unHealthy: ''
        ])
      },
      phpcs:{
        echo('Style')
        sh('ant phpcs-ci')
        // checkstyle publishing
        step([$class: "CheckStylePublisher",
          canComputeNew: false,
          defaultEncoding: "",
          healthy: "",
          pattern: "build/logs/checkstyle.xml",
          unHealthy: ""
        ])
      },
      phpcpd:{
        echo('CPD')
        sh('ant phpcpd-ci')
        // pmd publishing
        step([$class: 'DryPublisher',
          canComputeNew: false,
          defaultEncoding: '',
          healthy: '',
          pattern: 'build/logs/pmd-cpd.xml',
          unHealthy: ''
        ])
      },
      phpunit:{
        echo('Tests')
        sh('ant phpunit')
        junit 'build/logs/junit.xml'
        publishHTML([
          allowMissing: false,
          alwaysLinkToLastBuild: false,
          keepAll: false,
          reportDir: 'build/coverage',
          reportFiles: 'index.html',
          reportName: 'Coverage Html report',
          reportTitles: ''
        ])
        step([
          $class: 'CloverPublisher',
          cloverReportDir: 'build/coverage/',
          cloverReportFileName: 'clover.xml'
        ])
      }
    )
  }
}