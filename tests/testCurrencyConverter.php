<?php

namespace Test\Giift\CurrencyConverter;

use Giift\CurrencyConverter\Traits\HasLogger;
use Symfony\Component\Cache\Simple\NullCache;
use Giift\CurrencyConverter\Providers\Fixerio;
use Giift\CurrencyConverter\CurrencyConverter;
use Giift\CurrencyConverter\Providers\MockProvider;

/**
 * Class testCurrencyConverter
 * @package Test\Giift\CurrencyConverter
 * @group CurrencyConverter
 */
class testCurrencyConverter extends \PHPUnit_Framework_TestCase
{
    use HasLogger;

    /**
     * Testing with null cache and mock provider;
     */
    public function test1()
    {
        $nullCache = new NullCache();
        $mockProvider = new MockProvider($this->getLogger());
        $converter = new CurrencyConverter($this->getLogger(), $mockProvider, $nullCache);
        static::assertInstanceOf(CurrencyConverter::class, $converter);
        $rate = $converter->convert('USD', 'EUR');
        $this->validateRate($rate);
        static::assertEquals($rate, 1.00);
    }

    /**
     * Testing with config provider and nullcache
     */
    public function test2()
    {
        $nullCache = new NullCache();
        $converter = new CurrencyConverter($this->getLogger(), null, $nullCache);
        static::assertInstanceOf(CurrencyConverter::class, $converter);
        $rate = $converter->convert('USD', 'EUR', 50);
        $this->validateRate($rate);
        static::assertEquals($rate, 50.00);
    }

    public function test3()
    {
        $fixerio = new Fixerio($this->getLogger());
        $nullCache = new NullCache();
        $converter = new CurrencyConverter($this->getLogger(), $fixerio, $nullCache);
        $fixerioRate = $converter->convert('USD', 'EUR');
        $this->validateRate($fixerioRate);
    }

    public function test4()
    {
        $nullCache = new NullCache();
        $mockProvider = new MockProvider($this->getLogger());
        $mockProvider->setRate(0.077);
        $converter = new CurrencyConverter($this->getLogger(), $mockProvider, $nullCache);
        $rate = $converter->convert('USD', 'EUR');
        $this->validateRate($rate);
        static::assertEquals($rate, 0.077);
    }

    public function test5()
    {
        $nullCache = new NullCache();
        $mockProvider = new MockProvider($this->getLogger());
        $mockProvider->setRate(0.077);
        $converter = new CurrencyConverter($this->getLogger(), $mockProvider, $nullCache);
        $rate = $converter->convert('USD', 'EUR', 11.00);
        $this->validateRate($rate);
        static::assertEquals($rate, 0.077*11.00);
    }

    private function validateRate($rate)
    {
        static::assertTrue(is_float($rate));
        static::assertTrue(is_numeric($rate));
    }
}